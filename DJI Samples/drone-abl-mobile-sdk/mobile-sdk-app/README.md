# DJI Mobile SDK for Android Latest Version 4.16.2

Repositorio configurado y actualizado para el funcionamiento dentro del marco del proyecto del dron.

## Cambios realizados hasta la actualidad

- Se actualizó la dependencia del SDK de desarrollo (nativo android para compilar, no mobile sdk) objetivo de la aplicación de ejemplo: v31.0.0 -> v32.0.0
- Se actualizó la versión de Gradle de 4.2.2 a la 7.x.x utilizando el asistente automático de Android Studio.

## Errores encontrados y soluciones

### Error build-tools corruptas

Este error se da especificamente con la versión SDK-Manager V31, dado que en esta versión hay dos archivos importantes que tienen un nombre mal colocado, para solucionar se debe acceder a la carpeta del SDK de esta versión y renombrar los archivos "d8" a "dx", [referencia de solución](https://stackoverflow.com/questions/68387270/android-studio-error-installed-build-tools-revision-31-0-0-is-corrupted).

### Error al construir el menú, archivo XML no encontrado

De la siguiente [issue](https://github.com/dji-sdk/Mobile-SDK-Android/issues/1118) en el repositorio de Mobile SDK para Android, se indica que el SDK está diseñado para ser desarrollado, testeado y utilizado en dispositivos reales, por lo que será imposible hacer el desarrollo en emuladores.

## Como ejecutar el código de ejemplo

Primero se debe clonar el repositorio:

```bash
git clone https://gitlab.com/cemcc/drone-abl --branch mobile-sdk
```

Luego, con Android Studio, se debe abrir el proyecto de la mobile app, que es la carpeta `sdk-app`.
Para ejecutar la aplicación se necesita:

- Dispositivo android fisico, no emulador.
- SDK de desarrollo API 32 de android.

Finalmente con esto es posible ejecutar "run" y ver la aplicación como en la figura a continuación.

<details>
<summary>Screenshot de la aplicación</summary>
<img src="./app-screenshot.jpeg"></img>
</details>
